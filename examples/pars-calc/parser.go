package main

import (
	"bitbucket.org/ragnara/pars/v2"
	"fmt"
)

//ParseCalculation parses a calculation into an evaler.
func ParseCalculation(s string) (Evaler, error) {
	val, err := pars.ParseString(s, NewCalculationParser())
	if err != nil {
		return nil, err
	}
	return val.(Evaler), nil
}

//NewCalculationParser parses a calculation. After the calculation, EOF must occur.
func NewCalculationParser() pars.Parser {
	return pars.DiscardRight(NewTermParser(), pars.EOF)
}

//NewTermParser parses a calculation consisting of added or subtracted calculations of products or a single product.
func NewTermParser() pars.Parser {
	return newCombinedCalculationParser(NewProductParser(), newTermOperationClauses())
}

//NewProductParser parses a calculation consisting of multiplied or divided numbers or a single number.
func NewProductParser() pars.Parser {
	return newCombinedCalculationParser(NewNumberParser(), newProductOperationClauses())
}

func newProductOperationClauses() []pars.DispatchClause {
	return []pars.DispatchClause{
		newOperationClause('*', NewNumberParser(), "multiplication"),
		newOperationClause('/', NewNumberParser(), "division"),
	}
}

func newTermOperationClauses() []pars.DispatchClause {
	return []pars.DispatchClause{
		newOperationClause('+', NewProductParser(), "addition"),
		newOperationClause('-', NewProductParser(), "subtraction"),
	}
}

//NewNumberParser parses a single number.
func NewNumberParser() pars.Parser {
	return pars.Or(pars.Transformer(pars.SwallowWhitespace(pars.Float()), toNumber), pars.Error(fmt.Errorf("number expected")))
}

//NewOperatorParser parses the given rune as an operator.
func NewOperatorParser(r rune) pars.Parser {
	return pars.Transformer(pars.SwallowWhitespace(pars.Char(r)), toOperator)
}

func toOperator(v interface{}) (interface{}, error) {
	return getOperator(v.(rune)), nil
}

func toNumber(v interface{}) (interface{}, error) {
	return Number(v.(float64)), nil
}

func newCombinedCalculationParser(elemParser pars.Parser, operationClauses []pars.DispatchClause) pars.Parser {
	var left Evaler
	var operations []operation
	return pars.Transformer(
		pars.Dispatch(
			pars.Clause{pars.Into(elemParser, pars.AssignInto(&left)),
				pars.Into(pars.DispatchSome(operationClauses...), pars.AssignIntoSlice(&operations))}),
		func(_ interface{}) (interface{}, error) {
			return combineCalculation(left, operations), nil
		})
}

func newOperationClause(op rune, rhsParser pars.Parser, description string) pars.DispatchClause {
	o := operation{}
	return pars.InsteadOfDerefClause{
		DispatchClause: pars.DescribeClause{
			DispatchClause: pars.Clause{
				pars.Into(NewOperatorParser(op), pars.AssignInto(&o.op)),
				pars.Into(rhsParser, pars.AssignInto(&o.operand))},
			Description: description},
		Result: &o}
}

func combineCalculation(left Evaler, operations []operation) Evaler {
	result := left
	for _, operation := range operations {
		result = Calculation{a: result, op: operation.op, b: operation.operand}
	}

	return result
}

type operation struct {
	op      operator
	operand Evaler
}
